﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public Text recordText;
    public float oldrecord = 0f;
    public static float distance = 0;
    //Variables Guardadas
    public float recordDistance = 0;
    public int dinero;
    //-------------------------------------------
    public bool decrementing_state;

    void Start()
    {
        decrementing_state = true;
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString() + "m";
        gameOverText.gameObject.SetActive(false);
        oldrecord = recordDistance;
        LoadGame();
    }

    private void initial()
    {
        decrementing_state = true;
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
        oldrecord = recordDistance;
    }

    void Update()
    {
        GameOver();
        NewRecord();
        distanceText.text = distance.ToString() + "m";
        if (Restart.restart == true)
        {
            initial();
           
        }
    }
    IEnumerator KmDecrementing()
    {
        decrementing_state = !decrementing_state;
        if (decrementing_state == false)
        {
            yield return new WaitForSeconds(0.5f);
            if( Restart.restart == false)
            {
                distance += 1f;
                decrementing_state = !decrementing_state;
            }
           
        }
    }

    private void SaveGame()
    {
            SaveDataManager.SavePlayerData(this);
            Debug.Log("Datos guardados exitosamente...");
    }

    private void LoadGame() //*** Hay que hacerlo automatico ***
    {
        
            PlayerData playerData = SaveDataManager.LoadPlayerData();
            recordDistance = playerData.record;
            recordText.text = "Record: " + playerData.record.ToString() + "m";
            dinero = playerData.dinero;
            //Falta UI
            Debug.Log("Carga de datos exitosa...");
        
    }

    private void GameOver()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString();
            gameOverText.gameObject.SetActive(true);

            if (oldrecord < recordDistance)
            {
                SaveGame();
            }
        }
        else
        {
            if (decrementing_state == true && Restart.restart==false)
            {
                StartCoroutine(KmDecrementing());
            }
        }
    }

    private void NewRecord()
    {
        if (distance > recordDistance)
        {
            recordDistance = distance;
            recordText.text = "Record: " + distance.ToString() + "m";
        }
    }
}
