using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaftaCollision : MonoBehaviour
{
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            GestorDeAudio.instancia.ReproducirSonido("Energy");
            if (ProgressBar.Progress < 80)
            {
                ProgressBar.Progress += 20;
            } else
            {
                ProgressBar.Progress = 100;
            }
        }
    }
}
