using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEnemies : MonoBehaviour
{
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            this.gameObject.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("Hit");

        }
    }
}
