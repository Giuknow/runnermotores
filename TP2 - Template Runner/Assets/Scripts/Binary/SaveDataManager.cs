using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary; 
public static class SaveDataManager
{
    public static void SavePlayerData(Controller_Hud controller_Hud)
    {
        PlayerData playerData = new PlayerData(controller_Hud);
        string path = Application.persistentDataPath + "/jugador.save";
        FileStream fileStream = new FileStream(path, FileMode.Create);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(fileStream, playerData);
        fileStream.Close();
    }

    public static PlayerData LoadPlayerData()
    {
        string path = Application.persistentDataPath + "/jugador.save";

        if (File.Exists(path))
        {
            FileStream fileStream = new FileStream(path, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            PlayerData playerData = (PlayerData) binaryFormatter.Deserialize(fileStream);
            fileStream.Close();//       Casteo
            return playerData;
        }
        else
        {
            return null;
        }
    }
}
