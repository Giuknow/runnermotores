[System.Serializable]
public class PlayerData
{
    public int dinero;
    public float record;

    public PlayerData (Controller_Hud controller_Hud) //Constructor
    {
        record = controller_Hud.recordDistance;
        dinero = controller_Hud.dinero;
    }
}
