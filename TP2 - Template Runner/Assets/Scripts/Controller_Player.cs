﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 1f;
    private float initialSize; //TAMAÑO INICIAL = 0F *
    private int i = 0;
    private bool floored;
    public ParticleSystem fire1;
    public ParticleSystem fire2;

    private void Start()
    {
        fire1.Stop();
        fire2.Stop();
        rb = GetComponent<Rigidbody>(); // *
        initialSize = rb.transform.localScale.y; //SOLO TRANSFORMA EL EJE Y
    }
    private void initial()
    {
        fire1.Stop();
        fire2.Stop();
        rb = GetComponent<Rigidbody>(); // *
        initialSize = rb.transform.localScale.y; //SOLO TRANSFORMA EL EJE Y
    }

    void Update()
    {
        turnUmbrellaONOFF();
        GetInput();
        if (Restart.restart == true)
        {
            initial();
           
        }
    }

    private void GetInput()
    {
        Jump();
        Duck();
    }

    private void Jump()
    {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse); // SI ESTA EN EL SUELO LE DA UNA FUEZA DE SALTO (IMPULSO) IGUAL A 10 (VARIABLE jumpForce)
            } 
    }

    private void Duck()
    {
        if (floored) // A
        {
            if (Input.GetKey(KeyCode.S)) // B
            {
                if (i == 0) // C
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else // B
            {
                if (rb.transform.localScale.y != initialSize) // D
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else // A
        {
            if (Input.GetKeyDown(KeyCode.S))  // E
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
           this.gameObject.SetActive(false);
            Controller_Hud.gameOver = true;
            GestorDeAudio.instancia.ReproducirSonido("Hit");
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bus") == true)
        {
            GestorDeAudio.instancia.ReproducirSonido("Hit");
            other.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
            Controller_Hud.gameOver = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }

    private void turnUmbrellaONOFF ()
    {
        if (Input.GetKeyUp(KeyCode.W))
        {
            GestorDeAudio.instancia.PausarSonido("Fly");
            fire1.Stop();
            fire2.Stop();
        }

        if (Input.GetKeyDown(KeyCode.W)) 
        {
            GestorDeAudio.instancia.ReproducirSonido("Fly");
            fire1.Play();
            fire2.Play();
        } 
    }
}
