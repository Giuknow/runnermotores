﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void initial()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
        if (Restart.restart == true)
        {
            initial();
            
        }
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -25)
        {
            Destroy(this.gameObject);
        }
    }
}
