﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public static bool restart = false;
    public GameObject Jugador;
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R) && Controller_Hud.gameOver==true)
        {
            Time.timeScale = 1;
            restart = true;
            Controller_Hud.gameOver = false;
            Jugador.SetActive(true);

        }
        if (Input.GetKeyUp(KeyCode.R)&& Controller_Hud.gameOver==false)
        {
            restart = false;
        }
    }
}
 