﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 3f;
    }

    void Update()
    { 
        SpawnEnemies();
        // ChangeVelocity();
    }

    private void ChangeVelocity()
    {
       // time += Time.deltaTime;
        // Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 1)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(4, 7);
        }
    }
   
}
