using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Slider Bar;
    public static float Progress;

    void Start()
    {
        Progress = 100f;
    }
    private void initial()
    {
        Progress = 100f;
        Bar.value = Progress;
    }
   
    void Update()
    {
        Bar.GetComponent<Slider>().value = Progress;
        stopProgressBar();
        RanOutFuel();

        if (Restart.restart == true)
        {
            initial();
           
        }
    }

    private void stopProgressBar()
    {
        if (Controller_Hud.gameOver == false)
        {
            Progress -= 0.005f;
        } 
    }

    private void RanOutFuel()
    {
        if (Progress <= 0)
        {
            Controller_Hud.gameOver = true;
        }
    }

   
}
